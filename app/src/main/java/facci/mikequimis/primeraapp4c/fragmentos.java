package facci.mikequimis.primeraapp4c;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.*;
import android.widget.Button;

public class fragmentos extends AppCompatActivity implements View.OnClickListener,  frgUno.OnFragmentInteractionListener, frgDos.OnFragmentInteractionListener {

    Button Fragmentouno, Fragmentodos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentos);
      Fragmentouno= (Button) findViewById(R.id.button4);
      Fragmentodos= (Button) findViewById(R.id.button5);

        Fragmentouno.setOnClickListener(this);
        Fragmentodos.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button4:
                frgUno fragmentoUno = new frgUno();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,fragmentoUno);
                transaction.commit();
                break;
        }
        switch (v.getId()){
            case R.id.button5:
                frgDos fragmentoDos = new frgDos();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,fragmentoDos);
                transaction.commit();
                break;

             }
        }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
